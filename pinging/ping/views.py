from django.shortcuts import render
import os
from tcppinglib import tcpping
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, TemplateView, DetailView
from django.contrib.auth import logout, login
from ping.forms import RegisterUserForm, LoginUserForm, CreateLink
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from ping.models import *
# Create your views here.

def index(request):
    context ={}
    context['title']='пиганация'
    dat={}
    dat['aass']=22
    baz_d = Ping.objects.all()
    context['p']=0
    for i in baz_d:
        host = tcpping(i.link, interval=1.5)

        # and then check the response...
        if host.is_alive:
            i.report = 'пинганация прошла успешно'
            context['report']='пинганация прошла успешно'
            i.ip_address = host.ip_address
            i.min_rtt = host.min_rtt
            i.avg_rtt = host.avg_rtt
            i.packet_loss = host.packet_loss
        else:
            i.report = 'is down!'
            context['report'] = 'сайт не прошел пинганацию'
    context['baz_d'] = baz_d
    print(context['baz_d'])
    return render(request, 'baze.html', context)


def glavn(request):
    context ={}
    context['title']='пиганация'
    baz_d = Ping.objects.all()
    context['p']=0
    return render(request, 'glavn.html', context)

class Register( CreateView):
    form_class = RegisterUserForm
    template_name = 'register.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class SignIn( LoginView):
    form_class = LoginUserForm
    template_name = "sign_in.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return reverse_lazy('glavn')

def logout_user(request):
    logout(request)
    return redirect('glavn')

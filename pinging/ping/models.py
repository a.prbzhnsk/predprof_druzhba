from django.db import models

class Ping(models.Model):
    title = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    report = models.CharField(max_length=255)
    ip_address = models.CharField(max_length=255)
    min_rtt = models.CharField(max_length=255)
    avg_rtt = models.CharField(max_length=255)
    packet_loss = models.CharField(max_length=255)

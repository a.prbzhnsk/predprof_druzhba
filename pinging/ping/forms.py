from django import forms
from django.contrib.auth.models import User

from .models import *
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class RegisterUserForm(UserCreationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': "form-control form-control-lg"}))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': "form-control form-control-lg"}))
    password1 = forms.CharField(label='Пароль',
                                widget=forms.PasswordInput(attrs={'class': "form-control form-control-lg"}))
    password2 = forms.CharField(label='Повтор пароля',
                                widget=forms.PasswordInput(attrs={'class': "form-control form-control-lg"}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class LoginUserForm(AuthenticationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': "form-control form-control-lg"}))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': "form-control form-control-lg"}))


class CreateLink(forms.Form):
    template_name = "ping/templates/glavn.html"
    title = forms.CharField(label='Абревиатура')
    link = forms.CharField(label='Ссылка')


